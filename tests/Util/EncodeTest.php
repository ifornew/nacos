<?php

namespace Tests\Util;


use Tests\TestCase;
use Ifornew\Nacos\Util\EncodeUtil;

/**
 * Class EncodeTest
 * @author suxiaolin
 * @package tests\util
 */
class EncodeTest extends TestCase
{
    public function testOneEncode()
    {
        $this->assertNotEmpty(EncodeUtil::oneEncode());
    }

    public function testTwoEncode()
    {
        $this->assertNotEmpty(EncodeUtil::twoEncode());
    }
}
