<?php

namespace Tests\Util;


use Exception;
use Tests\TestCase;
use Ifornew\Nacos\Util\LogUtil;

/**
 * Class LogUtilTest
 * @author suxiaolin
 * @package tests\util
 */
class LogUtilTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testInfo()
    {
        $this->assertEmpty(LogUtil::info("info message"));
    }
}
