<?php

namespace Ifornew\Nacos\Request;

use ReflectionException;
use Ifornew\Nacos\NacosConfig;
use Ifornew\Nacos\Util\HttpUtil;
use Ifornew\Nacos\Enum\ErrorCodeEnum;
use Psr\Http\Message\ResponseInterface;
use Ifornew\Nacos\Exception\ResponseCodeErrorException;
use Ifornew\Nacos\Exception\RequestUriRequiredException;
use Ifornew\Nacos\Exception\RequestVerbRequiredException;

/**
 * Class Request
 * @author suxiaolin
 * @package Ifornew\Nacos\Request
 */
abstract class Request
{
    /**
     * 用户名
     * @var
     */
    protected $username;

    /**
     * 密码
     * @var
     */
    protected $password;

    /**
     * 接口地址
     * @var
     */
    protected $uri;

    /**
     * 接口动词
     * @var
     */
    protected $verb;

    /**
     * 忽略这些属性
     *
     * @var array
     */
    protected $standaloneParameterList = ["uri", "verb", "standaloneParameterList"];

    /**
     * 发起请求，做返回值异常检查
     *
     * @return mixed|ResponseInterface
     * @throws RequestUriRequiredException
     * @throws RequestVerbRequiredException
     * @throws ResponseCodeErrorException
     * @throws ReflectionException
     */
    public function doRequest()
    {
        list($parameterList, $headers) = $this->getParameterAndHeader();
        $response = HttpUtil::request(
            $this->getVerb(),
            $this->getUri(),
            $parameterList,
            $headers,
            ['debug' => NacosConfig::getIsDebug()]
        );

        if (isset(ErrorCodeEnum::getErrorCodeMap()[$response->getStatusCode()])) {
            throw new ResponseCodeErrorException($response->getStatusCode(), ErrorCodeEnum::getErrorCodeMap()[$response->getStatusCode()]);
        }
        return $response;
    }

    /**
     * 获取请求参数和请求头
     * @return array
     * @throws ReflectionException
     */
    abstract protected function getParameterAndHeader();

    /**
     * @return mixed
     * @throws
     */
    public function getVerb()
    {
        if ($this->verb == null) {
            throw new RequestVerbRequiredException();
        }
        return $this->verb;
    }

    /**
     * @param mixed $verb
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;
    }

    /**
     * @return mixed
     * @throws
     */
    public function getUri()
    {
        if ($this->uri == null) {
            throw new RequestUriRequiredException();
        }
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}
