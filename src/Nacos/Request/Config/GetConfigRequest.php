<?php

namespace Ifornew\Nacos\Request\Config;

/**
 * Class GetConfigRequest
 * @author suxiaolin
 * @package Ifornew\Nacos\Request\Config
 */
class GetConfigRequest extends ConfigRequest
{
    protected $uri = "/nacos/v1/cs/configs";
    protected $verb = "GET";

}