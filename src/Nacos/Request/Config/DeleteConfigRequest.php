<?php

namespace Ifornew\Nacos\Request\Config;

/**
 * Class DeleteConfigRequest
 * @author suxiaolin
 * @package Ifornew\Nacos\Request\Config
 */
class DeleteConfigRequest extends ConfigRequest
{
    protected $uri = "/nacos/v1/cs/configs";
    protected $verb = "DELETE";
}