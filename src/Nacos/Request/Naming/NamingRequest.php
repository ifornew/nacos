<?php


namespace Ifornew\Nacos\Request\Naming;


use Ifornew\Nacos\NacosConfig;
use Ifornew\Nacos\NamingConfig;
use Ifornew\Nacos\Util\LogUtil;
use Ifornew\Nacos\Request\Request;
use Ifornew\Nacos\Util\ReflectionUtil;

class NamingRequest extends Request
{

    protected function getParameterAndHeader()
    {
        $headers = [];
        $parameterList = [];

        $properties = ReflectionUtil::getProperties($this);
        foreach ($properties as $propertyName => $propertyValue) {
            if (in_array($propertyName, $this->standaloneParameterList)) {
                // 忽略这些参数
            } else {
                $parameterList[$propertyName] = $propertyValue;
            }
        }

        if ($this instanceof RegisterInstanceNaming || $this instanceof DeleteInstanceNaming ||
            $this instanceof UpdateInstanceNaming || $this instanceof ListInstanceNaming ||
            $this instanceof BeatInstanceNaming) {
            $parameterList["ephemeral"] = NamingConfig::getEphemeral();
        }

        if (NacosConfig::getIsDebug()) {
            LogUtil::info(strtr("parameterList: {parameterList}, headers: {headers}", [
                "parameterList" => json_encode($parameterList),
                "headers" => json_encode($headers)
            ]));
        }
        return [$parameterList, $headers];
    }

}