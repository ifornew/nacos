<?php


namespace Ifornew\Nacos\Exception;


use Exception;

/**
 * Class RequestUriRequiredException
 * @author suxiaolin
 * @package Ifornew\Nacos\Exception
 */
class RequestUriRequiredException extends Exception
{
    /**
     * RequestUriRequiredException constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}