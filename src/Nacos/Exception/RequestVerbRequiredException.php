<?php


namespace Ifornew\Nacos\Exception;


use Exception;

/**
 * Class RequestVerbRequiredException
 * @author suxiaolin
 * @package Ifornew\Nacos\Exception
 */
class RequestVerbRequiredException extends Exception
{
    /**
     * RequestVerbRequiredException constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}