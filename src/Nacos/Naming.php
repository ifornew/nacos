<?php


namespace Ifornew\Nacos;


use ReflectionException;
use Ifornew\Nacos\Model\Instance;

/**
 * Class Naming
 * @package Alibaba\Nacos
 */
class Naming
{
    /**
     * @param $serviceName
     * @param $ip
     * @param $port
     * @param string $namespaceId
     * @param string $weight
     * @return Naming
     */
    public static function init($serviceName, $ip, $port, $namespaceId = "", $weight = "", $ephemeral = true)
    {
        //static $client;
        //if ($client == null) {
            NamingConfig::setServiceName($serviceName);
            NamingConfig::setIp($ip);
            NamingConfig::setPort($port);
            NamingConfig::setNamespaceId($namespaceId);
            NamingConfig::setWeight($weight);
            NamingConfig::setEphemeral($ephemeral);

            $client = new self();
        //}
        return $client;
    }

    /**
     * @param bool $enable
     * @param bool $healthy
     * @param string $clusterName
     * @param string $metadata
     * @return bool
     * @throws ReflectionException
     * @throws exception\RequestUriRequiredException
     * @throws exception\RequestVerbRequiredException
     * @throws exception\ResponseCodeErrorException
     */
    public function register($enable = true, $healthy = true, $clusterName = "", $metadata = "{}")
    {
        return NamingClient::register(
            NamingConfig::getServiceName(),
            NamingConfig::getIp(),
            NamingConfig::getPort(),
            NamingConfig::getWeight(),
            NamingConfig::getNamespaceId(),
            $enable,
            $healthy,
            $clusterName,
            $metadata,
            NamingConfig::getUserName(),
            NamingConfig::getPassword()
        );
    }

    /**
     * @param $serviceName
     * @param $ip
     * @param $port
     * @param string $namespaceId
     * @param string $clusterName
     * @return bool
     * @throws ReflectionException
     * @throws exception\RequestUriRequiredException
     * @throws exception\RequestVerbRequiredException
     * @throws exception\ResponseCodeErrorException
     */
    public function delete($namespaceId = "", $clusterName = "")
    {
        return NamingClient::delete(
            NamingConfig::getServiceName(),
            NamingConfig::getIp(),
            NamingConfig::getPort(),
            $namespaceId,
            $clusterName,
            NamingConfig::getUserName(),
            NamingConfig::getPassword()
        );
    }

    /**
     * @param string $weight
     * @param string $namespaceId
     * @param string $clusterName
     * @param string $metadata
     * @return bool
     * @throws ReflectionException
     * @throws exception\RequestUriRequiredException
     * @throws exception\RequestVerbRequiredException
     * @throws exception\ResponseCodeErrorException
     */
    public function update($weight = "", $namespaceId = "", $clusterName = "", $metadata = "{}")
    {
        return NamingClient::update(
            NamingConfig::getServiceName(),
            NamingConfig::getIp(),
            NamingConfig::getPort(),
            $weight,
            $namespaceId,
            $clusterName,
            $metadata,
            NamingConfig::getUserName(),
            NamingConfig::getPassword()
        );
    }

    /**
     * @param bool $healthyOnly
     * @param string $namespaceId
     * @param string $clusters
     * @return model\InstanceList
     * @throws ReflectionException
     * @throws exception\RequestUriRequiredException
     * @throws exception\RequestVerbRequiredException
     * @throws exception\ResponseCodeErrorException
     */
    public function listInstances($healthyOnly = false, $namespaceId = "", $clusters = "")
    {
        return NamingClient::listInstances(
            NamingConfig::getServiceName(),
            $healthyOnly,
            $namespaceId,
            $clusters,
            NamingConfig::getUserName(),
            NamingConfig::getPassword()
        );
    }

    /**
     * @param Instance $instance
     * @return model\Beat
     * @throws ReflectionException
     * @throws exception\RequestUriRequiredException
     * @throws exception\RequestVerbRequiredException
     * @throws exception\ResponseCodeErrorException
     */
    public function beat(Instance $instance = null)
    {
        if ($instance == null) {
            //$instance = $this->get();//修改为以下两行
            $instance = $this->get(false,"",NamingConfig::getNamespaceId());
            $instance->setHealthy(true);
        }
        return NamingClient::beat(
            NamingConfig::getServiceName(),
            $instance->encode(),
            NamingConfig::getUserName(),
            NamingConfig::getPassword()
        );
    }

    /**
     * @param bool $healthyOnly
     * @param string $weight
     * @param string $namespaceId
     * @param string $clusters
     * @return model\Instance
     * @throws ReflectionException
     * @throws exception\RequestUriRequiredException
     * @throws exception\RequestVerbRequiredException
     * @throws exception\ResponseCodeErrorException
     */
    public function get($healthyOnly = false, $weight = "", $namespaceId = "", $clusters = "")
    {
        return NamingClient::get(
            NamingConfig::getServiceName(),
            NamingConfig::getIp(),
            NamingConfig::getPort(),
            $healthyOnly,
            $weight,
            $namespaceId,
            $clusters,
            NamingConfig::getUserName(),
            NamingConfig::getPassword()
        );
    }

}
