<?php


namespace Ifornew\Nacos;


use Ifornew\Nacos\Failover\LocalConfigInfoProcessor;

/**
 * Class DummyNacosClient
 * @author suxiaolin
 * @package Alibaba\Nacos
 */
class DummyNacosClient implements NacosClientInterface
{
    public static function get($env, $dataId, $group, $tenant, $username = null, $password = null)
    {
        $config = "";
        LocalConfigInfoProcessor::saveSnapshot($env, $dataId, $group, $tenant, $config);
        return $config;
    }

    public static function listener($env, $dataId, $group, $config, $tenant = "", $username = null, $password = null)
    {
        do {
            // 短暂休息会儿
            usleep(500);
        } while (true);
    }

    public static function publish($dataId, $group, $content, $tenant = "", $username = null, $password = null)
    {
        return true;
    }

    public static function delete($dataId, $group, $tenant, $username = null, $password = null)
    {
        return true;
    }
}
