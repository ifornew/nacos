<?php


namespace Ifornew\Nacos\Util;


/**
 * Class EncodeUtil
 * @author suxiaolin
 * @package Ifornew\Nacos\Util
 */
class EncodeUtil
{
    public static function twoEncode()
    {
        return pack("C*", 2);
    }

    public static function oneEncode()
    {
        return pack("C*", 1);
    }
}