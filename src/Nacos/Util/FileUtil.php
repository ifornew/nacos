<?php


namespace Ifornew\Nacos\Util;


/**
 * Class FileUtil
 * @author suxiaolin
 * @package Ifornew\Nacos\Util
 */
class FileUtil
{
    public static function deleteAll($path)
    {
        $files = glob("${$path}/*"); // get all file names
        foreach ($files as $file) { // iterate files
            if (!is_file($file)) {
                continue;
            }
            unlink($file); // delete file
        }
    }
}